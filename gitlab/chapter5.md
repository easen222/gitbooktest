## 查看文件網頁的網址

工具列點選「Setting」→「Pages」，就可以察知專案文檔的靜態網頁網址，預設一般為 [https://username.gitlab.io/project-name](https://username.gitlab.io/project-name)



![](/image/create-gitbook-project-at-gitlab-04.png)

