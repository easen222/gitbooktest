# 編輯書籍

在創建了書籍後，可以使用免費的在線編輯器進行編輯，也可以使用[gitbook editor](https://github.com/GitbookIO/editor)編輯，甚至使用任何喜歡的文本編輯器來編輯，例如：Vim。

## 在線編輯\(只支持GitHub\)

進入到書籍的屬性頁面後，點擊"Edit Book" 按鈕即可打開在線編輯器。

GitBook的在線編輯器對於國內用戶來說，很可能不能訪問，所以最好還是下載[gitbook editor](https://github.com/GitbookIO/editor)到本地，安裝後使用，或者使用自己喜歡的文本編輯器直接編輯。

## gitbook editor\(目前只能使用本地編輯\) {#gitbook-editor}

gitbook editor實際上就是一個本地應用版的在線編輯器，使用方式和在線編輯器類似，所見即所得，這裡不再介紹，讀者可以參考[gitbook使用](http://www.chengweiyang.cn/basic-usage/README.html)中的內容。

## Git & Markdown {#git--markdown}

另一種方式，是直接使用文本編輯器，編寫Markdown文檔，然後，使用Git提交到書籍的遠程項目，當然，提交前，最好在本地使用`gitbook`預覽效果；提交後，GitBook.com會自動生成更新書籍的內容。。

