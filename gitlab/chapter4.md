## 檢視 CI 執行情形

當新增上述檔案並點擊「Commit Change」後，GitLab CI 就會執行建置部屬的程序。在專案左側的工具列儀表板，點選「CI/CD」→「Jobs」，然後再點擊 Jobs 鍊結，即可出現 CI 執行紀錄，只要最後一行顯示

_Job succeeded_

就表示已正常完成建置部屬的工作。

![](/image/create-gitbook-project-at-gitlab-03.png)

