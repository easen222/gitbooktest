# 在 Windows 下安裝 Node.js

Windows 的使用者可以直接連上[Node.js 的官方網站](http://nodejs.org/)，在首頁就有一個「Install」按鈕，按下去就會下載適合自己作業系統的安裝檔。

  


# 在 Mac 下安裝 Node.js

#### **第一步** {#80ae}

使用 Homebrew 來安裝 nvm 

```
$ brew install nvm
```

接下來

先確認有沒有這個檔案沒有的話先建立

/Users/YourMacUserName/.bash\_profile

```
$ echo "source $(brew --prefix nvm)/nvm.sh" >> .bash_profile
$ . ~/.bash_profile
```

#### 第二步 {#8ea1}

看有哪些版本可以被安裝。

`$ nvm ls-remote`

  
指定想要的版本

`$ nvm install <version>`

也可以直接安裝目前的穩定版

`$ nvm install stable`

#### 第三步

使用自己想要的版本

`$ nvm use <version>`










