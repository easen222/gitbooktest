# Summary

## 簡介

* [說明](README.md)

## 一、基本安裝

* [安裝Node.Js](install/Node.js.md)
* [安裝GitBook](install/GitBook.md)

## 二、GitBook基本架構

* [目錄](/gitbook/Dir.md)
* [編輯方式](gitbook/EditWays.md)

## 三、使用Gitlab

* [新增專案](gitlab/chapter1.md)
* [增加CI/CD](gitlab/chapter2.md)
* [新增檔案](/gitlab/chapter3.md)
* [檢視CI執行情形](/gitlab/chapter4.md)
* [檢視文件網址](/gitlab/chapter5.md)



