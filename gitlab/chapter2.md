# 第二步驟設定 CI/CD

新增專案後在出現的主視窗內，點選「Set up CI/CD」按鈕，即會出現預設檔名為「.gitlab-ci.yml」的編輯視窗，將 GitLab 所提供在 GitBook 儲庫內的「[.gitlab-ci.yml](https://gitlab.com/pages/gitbook/blob/master/.gitlab-ci.yml)」檔案內容複製並貼上，並點選「Commit Change」即可完成設定。

![](/image/create-gitbook-project-at-gitlab-02.png)

```
# requiring the environment of NodeJS 10
image: node:10

# add 'node_modules' to cache for speeding up builds
cache:
  paths:
    - node_modules/ # Node modules and dependencies

before_script:
  - npm install gitbook-cli -g # install gitbook
  - gitbook fetch 3.2.3 # fetch final stable version
  - gitbook install # add any requested plugins in book.json

test:
  stage: test
  script:
    - gitbook build . public # build to public path
  only:
    - branches # this job will affect every branch except 'master'
  except:
    - master

# the 'pages' job will deploy and build your site to the 'public' path
pages:
  stage: deploy
  script:
    - gitbook build . public # build to public path
  artifacts:
    paths:
      - public
    expire_in: 1 week
  only:
    - master # this job will affect only the 'master' branch
```



